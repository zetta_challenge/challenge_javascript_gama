// Expected result : [7, 3, 1, 2, 5, 6, 9, 10, 4, 8]
// Direction : Mutate arr1 to return combined array with arr2. The conditions are :
// 1. odd number at beginning
// 2. even number at the end of array
// 3. Original arr1 at the middle

const arr1 = [1, 2, 5, 6, 9, 10]
const arr2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

function result(arr1, arr2) {
  let arr5 = []
  let arr3 = arr1.concat(arr2)
  let arr4 = arr3.filter((item, pos) => arr3.indexOf(item) === pos)

  for (let i = 0; i < arr4.length; i++) {
    if (arr4[i] % 2 === 0) {
      arr5.push(arr4[i])
    } else {
      arr5.unshift(arr4[i])
    }
  }

  return arr5
}

console.log(result(arr1, arr2))
