// Expected Result : [false, true]
// Direction :
// The first value : If all of arr2 has bigger value than the biggest value of arr1;
// The second value : If some of arr2 has smaller value than the smallest value of arr1;
const arr1 = [4, 6, 2, 3, 5]
const arr2 = [1, 3, 4, 7, 9, 10]

function result(arr1, arr2) {
  let a = []
  let b = []

  for (i = 0; i < arr2.length; i++) {
    if (arr2[i] < Math.max(...arr1)) {
      a.push(false)
    } else {
      a.push(true)
    }
  }

  for (i = 0; i < arr2.length; i++) {
    if (arr2[i] < Math.min(...arr1)) {
      b.push(true)
    } else {
      b.push(false)
    }
  }

  let c = a.includes(false) ? false : true
  let d = b.includes(true) ? true : false

  // Your Code Here
  const res = [c, d]
  return res
}

console.log(result(arr1, arr2))
