// Expected Result : 6
// Direction : Get the total of "1" in binary value of number input
const number = 221

function result(num) {
  // Your Code Here
  const dec2bin = num => {
    return (num >>> 0).toString(2)
  }
  digits = ('' + dec2bin(num)).split('').map(Number)

  return digits.reduce(
    (accumulator, currentValue) => accumulator + currentValue
  )
}

console.log(result(number))
